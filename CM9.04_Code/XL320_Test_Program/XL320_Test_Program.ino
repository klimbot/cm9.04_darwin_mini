/* Dynamixel Basic Position Control Example
 
 Turns left the dynamixel , then turn right for one second,
 repeatedly.
 
                   Compatibility
 CM900                  O
 OpenCM9.04             O
 
                  Dynamixel Compatibility
               AX    MX      RX    XL-320    Pro
 CM900          O      O      O        O      X
 OpenCM9.04     O      O      O        O      X
 **** OpenCM 485 EXP board is needed to use 4 pin Dynamixel and Pro Series ****
 
 created 16 Nov 2012
 by ROBOTIS CO,.LTD.
 */
/* Serial device defines for dxl bus */
#define DXL_BUS_SERIAL1 1  //Dynamixel on Serial1(USART1)  <-OpenCM9.04

/* Dynamixel ID defines */
#define START_ID_NUM 1
#define NUM_SERVOS 16

/* Control table defines */
#define GOAL_POSITION 30
//#define P_PRESENT_POSITION_L 37

Dynamixel Dxl(DXL_BUS_SERIAL1);

void setup() {
  delay(3000);
  SerialUSB.begin();

  SerialUSB.print("P_PRESENT_POSITION_L:");
  SerialUSB.println(P_PRESENT_POSITION_L);

  SerialUSB.print("COMM_RXSUCCESS:");
  SerialUSB.println(COMM_RXSUCCESS);
   
  // Dynamixel 2.0 Baudrate -> 0: 9600, 1: 57600, 2: 115200, 3: 1Mbps 
  Dxl.begin(3);
  
  for(int i = START_ID_NUM ; i < (START_ID_NUM+NUM_SERVOS) ; i++) {
    Dxl.jointMode(i); //jointMode() is to use position mode
  }
}

void loop() {  
  for(int i = START_ID_NUM ; i < (START_ID_NUM+NUM_SERVOS) ; i++) {
    //Move dynamixel
    Dxl.writeWord(i, GOAL_POSITION, 500); //Compatible with all dynamixel series
    
    // Wait for 1 second (1000 milliseconds)
    delay(1000);
    
    SerialUSB.print("Pos:");
    SerialUSB.println(Dxl.readWord(i, 37));
    
    SerialUSB.print("Result:");
    SerialUSB.println(Dxl.getResult());
    
    // Wait for 1 second (1000 milliseconds)
    delay(1000);
    
    //Move dynamixel
    Dxl.writeWord(i, GOAL_POSITION, 600);
    
    // Wait for 1 second (1000 milliseconds)
    delay(1000);
  }
}
