/* Dynamixel Basic Position Control Example
 
 Turns left the dynamixel , then turn right for one second,
 repeatedly.
 
                   Compatibility
 CM900                  O
 OpenCM9.04             O
 
                  Dynamixel Compatibility
               AX    MX      RX    XL-320    Pro
 CM900          O      O      O        O      X
 OpenCM9.04     O      O      O        O      X
 **** OpenCM 485 EXP board is needed to use 4 pin Dynamixel and Pro Series ****
 
 created 16 Nov 2012
 by ROBOTIS CO,.LTD.
 */

#include <CM9_BC.h>
/* Serial device defines for dxl bus */
#define DXL_BUS_SERIAL1 1  //Dynamixel on Serial1(USART1)  <-OpenCM9.04

/* Dynamixel ID defines */
#define START_ID_NUM 1
#define NUM_SERVOS 16

/* Control table defines */
#define GOAL_POSITION 30
#define P_PRESENT_POSITION_L 37

Dynamixel Dxl(DXL_BUS_SERIAL1);
Dynamixel *pDxl = &Dxl;

unsigned int servoID = 15;

void setup() {
  delay(3000);
  SerialUSB.begin();

  // Dynamixel 2.0 Baudrate -> 0: 9600, 1: 57600, 2: 115200, 3: 1Mbps 
  pDxl->begin(3);
  pDxl->jointMode(BROADCAST_ID);
  
  // 
  pDxl->writeWord(servoID, GOAL_POSITION, 512);
}

void loop() { 
  unsigned int inputChar[5] = {0,0,0,0,0};
  unsigned int numChars = 0;
  
  
  // Read the Serial USB port for a command  
  while (SerialUSB.available()) {
    inputChar[numChars] = SerialUSB.read();
    numChars++;
  }
  
  if(inputChar[0] == '+') {
    increasePosition(servoID);
  }
  else if(inputChar[0] == '-') {
    decreasePosition(servoID);
  }
  
  
  /*
  switch (something) {
    case '1':
      //something
      break;
    case '2':
    
  
  for(int i = 0 ; i < numChars ; i++) {
    
  }
  	
  // KEY OPTIONS
  if (rang == '1') {
    servoID = 1;
  }
  else if (rang == '2') {
    servoID = 2;
  }
  else if (rang == '3') {
    servoID = 3;
  }
  */
  delay(500);
}

void increasePosition(int servoNum) {
  int pos = Dxl.readWord(servoNum, P_PRESENT_POSITION_L);
  Dxl.writeWord(servoNum, GOAL_POSITION, pos+10);
}

void decreasePosition(int servoNum) {
  int pos = Dxl.readWord(servoNum, P_PRESENT_POSITION_L);
  Dxl.writeWord(servoNum, GOAL_POSITION, pos-10);
}
