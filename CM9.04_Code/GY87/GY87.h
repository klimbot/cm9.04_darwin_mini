/**
 * Cut down and mashed up version of the library written by jarzebski
 * 
 * https://github.com/jarzebski/Arduino-MPU6050
 * https://github.com/jarzebski/Arduino-HMC5883L
 */

#ifndef GY87_h
#define GY87_h

// MPU6050 Gyroscope definitions
#define MPU6050_ADDRESS               (0x68) // 0x69 when AD0 pin to Vcc
#define MPU6050_REG_ACCEL_XOFFS_H     (0x06)
#define MPU6050_REG_ACCEL_XOFFS_L     (0x07)
#define MPU6050_REG_ACCEL_YOFFS_H     (0x08)
#define MPU6050_REG_ACCEL_YOFFS_L     (0x09)
#define MPU6050_REG_ACCEL_ZOFFS_H     (0x0A)
#define MPU6050_REG_ACCEL_ZOFFS_L     (0x0B)
#define MPU6050_REG_GYRO_XOFFS_H      (0x13)
#define MPU6050_REG_GYRO_XOFFS_L      (0x14)
#define MPU6050_REG_GYRO_YOFFS_H      (0x15)
#define MPU6050_REG_GYRO_YOFFS_L      (0x16)
#define MPU6050_REG_GYRO_ZOFFS_H      (0x17)
#define MPU6050_REG_GYRO_ZOFFS_L      (0x18)
#define MPU6050_REG_CONFIG            (0x1A)
#define MPU6050_REG_GYRO_CONFIG       (0x1B) // Gyroscope Configuration
#define MPU6050_REG_ACCEL_CONFIG      (0x1C) // Accelerometer Configuration
#define MPU6050_REG_FF_THRESHOLD      (0x1D)
#define MPU6050_REG_FF_DURATION       (0x1E)
#define MPU6050_REG_MOT_THRESHOLD     (0x1F)
#define MPU6050_REG_MOT_DURATION      (0x20)
#define MPU6050_REG_ZMOT_THRESHOLD    (0x21)
#define MPU6050_REG_ZMOT_DURATION     (0x22)
#define MPU6050_REG_INT_PIN_CFG       (0x37) // INT Pin. Bypass Enable Configuration
#define MPU6050_REG_INT_ENABLE        (0x38) // INT Enable
#define MPU6050_REG_INT_STATUS        (0x3A)
#define MPU6050_REG_ACCEL_XOUT_H      (0x3B)
#define MPU6050_REG_ACCEL_XOUT_L      (0x3C)
#define MPU6050_REG_ACCEL_YOUT_H      (0x3D)
#define MPU6050_REG_ACCEL_YOUT_L      (0x3E)
#define MPU6050_REG_ACCEL_ZOUT_H      (0x3F)
#define MPU6050_REG_ACCEL_ZOUT_L      (0x40)
#define MPU6050_REG_TEMP_OUT_H        (0x41)
#define MPU6050_REG_TEMP_OUT_L        (0x42)
#define MPU6050_REG_GYRO_XOUT_H       (0x43)
#define MPU6050_REG_GYRO_XOUT_L       (0x44)
#define MPU6050_REG_GYRO_YOUT_H       (0x45)
#define MPU6050_REG_GYRO_YOUT_L       (0x46)
#define MPU6050_REG_GYRO_ZOUT_H       (0x47)
#define MPU6050_REG_GYRO_ZOUT_L       (0x48)
#define MPU6050_REG_MOT_DETECT_STATUS (0x61)
#define MPU6050_REG_MOT_DETECT_CTRL   (0x69)
#define MPU6050_REG_USER_CTRL         (0x6A) // User Control
#define MPU6050_REG_PWR_MGMT_1        (0x6B) // Power Management 1
#define MPU6050_REG_WHO_AM_I          (0x75) // Who Am I

// HCM5883L Magnetometer definitions
#define HMC5883L_ADDRESS              (0x1E)
#define HMC5883L_REG_CONFIG_A         (0x00)
#define HMC5883L_REG_CONFIG_B         (0x01)
#define HMC5883L_REG_MODE             (0x02)
#define HMC5883L_REG_OUT_X_M          (0x03)
#define HMC5883L_REG_OUT_X_L          (0x04)
#define HMC5883L_REG_OUT_Z_M          (0x05)
#define HMC5883L_REG_OUT_Z_L          (0x06)
#define HMC5883L_REG_OUT_Y_M          (0x07)
#define HMC5883L_REG_OUT_Y_L          (0x08)
#define HMC5883L_REG_STATUS           (0x09)
#define HMC5883L_REG_IDENT_A          (0x0A)
#define HMC5883L_REG_IDENT_B          (0x0B)
#define HMC5883L_REG_IDENT_C          (0x0C)


#ifndef VECTOR_STRUCT_H
#define VECTOR_STRUCT_H
struct Vector
{
    float XAxis;
    float YAxis;
    float ZAxis;
};
#endif

struct Activites
{
    bool isOverflow;
    bool isFreeFall;
    bool isInactivity;
    bool isActivity;
    bool isPosActivityOnX;
    bool isPosActivityOnY;
    bool isPosActivityOnZ;
    bool isNegActivityOnX;
    bool isNegActivityOnY;
    bool isNegActivityOnZ;
    bool isDataReady;
};

typedef enum
{
    MPU6050_CLOCK_KEEP_RESET      = 0b111,
    MPU6050_CLOCK_EXTERNAL_19MHZ  = 0b101,
    MPU6050_CLOCK_EXTERNAL_32KHZ  = 0b100,
    MPU6050_CLOCK_PLL_ZGYRO       = 0b011,
    MPU6050_CLOCK_PLL_YGYRO       = 0b010,
    MPU6050_CLOCK_PLL_XGYRO       = 0b001,
    MPU6050_CLOCK_INTERNAL_8MHZ   = 0b000
} mpu6050_clockSource_t;

typedef enum
{
    MPU6050_SCALE_2000DPS         = 0b11,
    MPU6050_SCALE_1000DPS         = 0b10,
    MPU6050_SCALE_500DPS          = 0b01,
    MPU6050_SCALE_250DPS          = 0b00
} mpu6050_dps_t;

typedef enum
{
    MPU6050_RANGE_16G             = 0b11,
    MPU6050_RANGE_8G              = 0b10,
    MPU6050_RANGE_4G              = 0b01,
    MPU6050_RANGE_2G              = 0b00,
} mpu6050_range_t;

typedef enum
{
    MPU6050_DELAY_3MS             = 0b11,
    MPU6050_DELAY_2MS             = 0b10,
    MPU6050_DELAY_1MS             = 0b01,
    MPU6050_NO_DELAY              = 0b00,
} mpu6050_onDelay_t;

typedef enum
{
    MPU6050_DHPF_HOLD             = 0b111,
    MPU6050_DHPF_0_63HZ           = 0b100,
    MPU6050_DHPF_1_25HZ           = 0b011,
    MPU6050_DHPF_2_5HZ            = 0b010,
    MPU6050_DHPF_5HZ              = 0b001,
    MPU6050_DHPF_RESET            = 0b000,
} mpu6050_dhpf_t;

typedef enum
{
    MPU6050_DLPF_6                = 0b110,
    MPU6050_DLPF_5                = 0b101,
    MPU6050_DLPF_4                = 0b100,
    MPU6050_DLPF_3                = 0b011,
    MPU6050_DLPF_2                = 0b010,
    MPU6050_DLPF_1                = 0b001,
    MPU6050_DLPF_0                = 0b000,
} mpu6050_dlpf_t;

typedef enum
{
    HMC5883L_SAMPLES_8     = 0b11,
    HMC5883L_SAMPLES_4     = 0b10,
    HMC5883L_SAMPLES_2     = 0b01,
    HMC5883L_SAMPLES_1     = 0b00
} hmc5883l_samples_t;

typedef enum
{
    HMC5883L_DATARATE_75HZ       = 0b110,
    HMC5883L_DATARATE_30HZ       = 0b101,
    HMC5883L_DATARATE_15HZ       = 0b100,
    HMC5883L_DATARATE_7_5HZ      = 0b011,
    HMC5883L_DATARATE_3HZ        = 0b010,
    HMC5883L_DATARATE_1_5HZ      = 0b001,
    HMC5883L_DATARATE_0_75_HZ    = 0b000
} hmc5883l_dataRate_t;

typedef enum
{
    HMC5883L_RANGE_8_1GA     = 0b111,
    HMC5883L_RANGE_5_6GA     = 0b110,
    HMC5883L_RANGE_4_7GA     = 0b101,
    HMC5883L_RANGE_4GA       = 0b100,
    HMC5883L_RANGE_2_5GA     = 0b011,
    HMC5883L_RANGE_1_9GA     = 0b010,
    HMC5883L_RANGE_1_3GA     = 0b001,
    HMC5883L_RANGE_0_88GA    = 0b000
} hmc5883l_range_t;

typedef enum
{
    HMC5883L_IDLE          = 0b10,
    HMC5883L_SINGLE        = 0b01,
    HMC5883L_CONTINOUS     = 0b00
} hmc5883l_mode_t;



class GY87
{
  public:
    // GY87 Functions
    bool gy87Begin(mpu6050_dps_t mpuScale = MPU6050_SCALE_2000DPS, mpu6050_range_t mpuRange = MPU6050_RANGE_2G, int mpuAddress = MPU6050_ADDRESS, hmc5883l_range_t magRange = HMC5883L_RANGE_1_3GA, hmc5883l_mode_t magMode = HMC5883L_CONTINOUS, hmc5883l_dataRate_t magRate = HMC5883L_DATARATE_30HZ, hmc5883l_samples_t magSamples = HMC5883L_SAMPLES_8);
    
    // MPU6050 Gyroscope Functions
    bool mpuBegin(mpu6050_dps_t scale = MPU6050_SCALE_2000DPS, mpu6050_range_t range = MPU6050_RANGE_2G, int mpua = MPU6050_ADDRESS);
    void mpuSetClockSource(mpu6050_clockSource_t source);
    mpu6050_clockSource_t mpuGetClockSource(void);
    
    void mpuSetScale(mpu6050_dps_t scale);
    mpu6050_dps_t mpuGetScale(void);
    void mpuSetRange(mpu6050_range_t range);
    mpu6050_range_t mpuGetRange(void);
    
    void mpuSetDHPFMode(mpu6050_dhpf_t dhpf);
    void mpuSetDLPFMode(mpu6050_dlpf_t dlpf);
    mpu6050_onDelay_t mpuGetAccelPowerOnDelay();
    void mpuSetAccelPowerOnDelay(mpu6050_onDelay_t delay);
    uint8 mpuGetIntStatus(void);
    bool mpuGetIntZeroMotionEnabled(void);
    void mpuSetIntZeroMotionEnabled(bool state);
    bool mpuGetIntMotionEnabled(void);
    void mpuSetIntMotionEnabled(bool state);
    bool mpuGetIntFreeFallEnabled(void);
    void mpuSetIntFreeFallEnabled(bool state);
    uint8 mpuGetMotionDetectionThreshold(void);
    void mpuSetMotionDetectionThreshold(uint8 threshold);
    uint8 mpuGetMotionDetectionDuration(void);
    void mpuSetMotionDetectionDuration(uint8 duration);
    uint8 mpuGetZeroMotionDetectionThreshold(void);
    void mpuSetZeroMotionDetectionThreshold(uint8 threshold);
    uint8 mpuGetZeroMotionDetectionDuration(void);
    void mpuSetZeroMotionDetectionDuration(uint8 duration);
    uint8 mpuGetFreeFallDetectionThreshold(void);
    void mpuSetFreeFallDetectionThreshold(uint8 threshold);
    uint8 mpuGetFreeFallDetectionDuration(void);
    void mpuSetFreeFallDetectionDuration(uint8 duration);
    bool mpuGetSleepEnabled(void);
    void mpuSetSleepEnabled(bool state);
    bool mpuGetI2CMasterModeEnabled(void);
    void mpuSetI2CMasterModeEnabled(bool state);
    bool mpuGetI2CBypassEnabled(void);
    void mpuSetI2CBypassEnabled(bool state);
    
    Activites mpuReadActivites(void);
    int16 mpuGetGyroOffsetX(void);
    void mpuSetGyroOffsetX(int16 offset);
    int16 mpuGetGyroOffsetY(void);
    void mpuSetGyroOffsetY(int16 offset);
    int16 mpuGetGyroOffsetZ(void);
    void mpuSetGyroOffsetZ(int16 offset);
    int16 mpuGetAccelOffsetX(void);
    void mpuSetAccelOffsetX(int16 offset);
    int16 mpuGetAccelOffsetY(void);
    void mpuSetAccelOffsetY(int16 offset);
    int16 mpuGetAccelOffsetZ(void);
    void mpuSetAccelOffsetZ(int16 offset);
    
    void mpuCalibrateGyro(uint8 samples = 50);
    void mpuSetThreshold(uint8 multiple = 1);
    uint8 mpuGetThreshold(void);
    
    Vector mpuReadRawGyro(void);
    Vector mpuReadNormalizeGyro(void);
    
    Vector mpuReadRawAccel(void);
    Vector mpuReadNormalizeAccel(void);
    Vector mpuReadScaledAccel(void);
    
    float mpuReadTemperature(void);
    
    
    
    // HCM5883L Magnetometer Functions
    bool magBegin(void);
    Vector magReadRaw(void);
    Vector magReadNormalize(void);
    void  magSetOffset(int xo, int yo);
    
    void  magSetRange(hmc5883l_range_t range);
    hmc5883l_range_t magGetRange(void);
    void  magSetMeasurementMode(hmc5883l_mode_t mode);
    hmc5883l_mode_t magGetMeasurementMode(void);
    void  magSetDataRate(hmc5883l_dataRate_t dataRate);
    hmc5883l_dataRate_t magGetDataRate(void);
    void  magSetSamples(hmc5883l_samples_t samples);
    hmc5883l_samples_t magGetSamples(void);

  private:
    // MPU6050 Gyroscope
    Vector mpuRa, mpuRg; // Raw vectors
    Vector mpuNa, mpuNg; // Normalized vectors
    Vector mpuTg, mpuDg; // Threshold and Delta for Gyro
    Vector mpuTh;         // Threshold
    Activites mpuA;       // Activities
    float mpuDpsPerDigit, mpuRangePerDigit;
    float mpuActualThreshold;
    bool mpuUseCalibrate;
    int mpuAddress;
    uint8 mpuFastRegister8(uint8 reg);
    uint8 mpuReadRegister8(uint8 reg);
    void mpuWriteRegister8(uint8 reg, uint8 value);
    int16 mpuReadRegister16(uint8 reg);
    void mpuWriteRegister16(uint8 reg, int16 value);
    bool mpuReadRegisterBit(uint8 reg, uint8 pos);
    void mpuWriteRegisterBit(uint8 reg, uint8 pos, bool state);
  
    // HMC5883 Magnetometer
    float magGPerDigit;
    Vector magMV;
    int magXOffset, magYOffset;
    void magWriteRegister8(uint8_t reg, uint8_t value);
    uint8 magReadRegister8(uint8_t reg);
    uint8 magFastRegister8(uint8_t reg);
    int16 magReadRegister16(uint8_t reg);
};

#endif
