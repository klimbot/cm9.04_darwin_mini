#include <Wire.h>
#include <GY87.h>

GY87 darwinIMU;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setup() {
  delay(3000);
  SerialUSB.begin();
  
  Wire.begin(25,24);    //i2c bus init SDA->25, SCL->24
  SerialUSB.println("Setup started");
  
  // Initialise the GY87
  SerialUSB.println("Initialising GY87");
  while(!darwinIMU.gy87Begin(MPU6050_SCALE_1000DPS, MPU6050_RANGE_2G, MPU6050_ADDRESS, HMC5883L_RANGE_1_3GA, HMC5883L_CONTINOUS, HMC5883L_DATARATE_30HZ, HMC5883L_SAMPLES_8)) { delay(500); }
  //while(!darwinIMU.gy87Begin()) { delay(500); }
  
  SerialUSB.println("Mag set offset");
  darwinIMU.magSetOffset(0,0);
  
  SerialUSB.println("Setup complete");
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void loop() {
  //float temp = darwinIMU.mpuReadTemperature();  
  //Vector accelRaw = darwinIMU.mpuReadRawAccel();
  Vector gyroRaw = darwinIMU.mpuReadRawGyro();
  //Vector magRaw = darwinIMU.magReadRaw();
  
  /*
  SerialUSB.print(" ACCEL: Xraw = ");
  SerialUSB.print(accelRaw.XAxis);
  SerialUSB.print(" Yraw = ");
  SerialUSB.print(accelRaw.YAxis);
  SerialUSB.print(" Zraw = ");
  SerialUSB.print(accelRaw.ZAxis);
  */
  
  SerialUSB.print(" GYRO: Xraw = ");
  SerialUSB.print(gyroRaw.XAxis);
  SerialUSB.print(" Yraw = ");
  SerialUSB.print(gyroRaw.YAxis);
  SerialUSB.print(" Zraw = ");
  SerialUSB.print(gyroRaw.ZAxis);
  
  /*
  SerialUSB.print(" MAG: Xraw = ");
  SerialUSB.print(magRaw.XAxis);
  SerialUSB.print(" Yraw = ");
  SerialUSB.print(magRaw.YAxis);
  SerialUSB.print(" Zraw = ");
  SerialUSB.print(magRaw.ZAxis);
  */
  /*
  SerialUSB.print(" TEMP = ");
  SerialUSB.print(temp);
  SerialUSB.print(" *C");
  */
  SerialUSB.println();
  //delay(50);
}

