/**
 * Cut down and mashed up version of the library written by jarzebski
 * 
 * https://github.com/jarzebski/Arduino-MPU6050
 * https://github.com/jarzebski/Arduino-HMC5883L
 */

#include <Wire.h>
#include <GY87.h>

bool GY87::gy87Begin(mpu6050_dps_t mpuScale, mpu6050_range_t mpuRange, int mpuAddress, hmc5883l_range_t magRange, hmc5883l_mode_t magMode, hmc5883l_dataRate_t magRate, hmc5883l_samples_t magSamples)
{
  if(mpuBegin(mpuScale, mpuRange, mpuAddress)) {
    mpuSetI2CMasterModeEnabled(false);
    mpuSetI2CBypassEnabled(true);
    mpuSetSleepEnabled(false);
    
    if(magBegin()) {
      magSetRange(magRange);
      magSetMeasurementMode(magMode);
      magSetDataRate(magRate);
      magSetSamples(magSamples);
      
      return true;
    }
  }
  
  return false;
}

bool GY87::mpuBegin(mpu6050_dps_t scale, mpu6050_range_t range, int mpua)
{
    // Set Address
    mpuAddress = mpua;

    //Wire.begin();

    // Reset calibrate values
    mpuDg.XAxis = 0;
    mpuDg.YAxis = 0;
    mpuDg.ZAxis = 0;
    mpuUseCalibrate = false;

    // Reset threshold values
    mpuTg.XAxis = 0;
    mpuTg.YAxis = 0;
    mpuTg.ZAxis = 0;
    mpuActualThreshold = 0;

    // Check MPU6050 Who Am I Register
    if (mpuFastRegister8(MPU6050_REG_WHO_AM_I) != 0x68)
    {
      return false;
    }

    // Set Clock Source
    mpuSetClockSource(MPU6050_CLOCK_PLL_XGYRO);

    // Set Scale & Range
    mpuSetScale(scale);
    mpuSetRange(range);

    // Disable Sleep Mode
    mpuSetSleepEnabled(false);

    return true;
}

void GY87::mpuSetScale(mpu6050_dps_t scale)
{
    uint8 value;

    switch (scale)
    {
	case MPU6050_SCALE_250DPS:
	    mpuDpsPerDigit = .007633f;
	    break;
	case MPU6050_SCALE_500DPS:
	    mpuDpsPerDigit = .015267f;
	    break;
	case MPU6050_SCALE_1000DPS:
	    mpuDpsPerDigit = .030487f;
	    break;
	case MPU6050_SCALE_2000DPS:
	    mpuDpsPerDigit = .060975f;
	    break;
	default:
	    break;
    }

    value = mpuReadRegister8(MPU6050_REG_GYRO_CONFIG);
    value &= 0b11100111;
    value |= (scale << 3);
    mpuWriteRegister8(MPU6050_REG_GYRO_CONFIG, value);
}

mpu6050_dps_t GY87::mpuGetScale(void)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_GYRO_CONFIG);
    value &= 0b00011000;
    value >>= 3;
    return (mpu6050_dps_t)value;
}

void GY87::mpuSetRange(mpu6050_range_t range)
{
    uint8 value;

    switch (range)
    {
	case MPU6050_RANGE_2G:
	    mpuRangePerDigit = .000061f;
	    break;
	case MPU6050_RANGE_4G:
	    mpuRangePerDigit = .000122f;
	    break;
	case MPU6050_RANGE_8G:
	    mpuRangePerDigit = .000244f;
	    break;
	case MPU6050_RANGE_16G:
	    mpuRangePerDigit = .0004882f;
	    break;
	default:
	    break;
    }

    value = mpuReadRegister8(MPU6050_REG_ACCEL_CONFIG);
    value &= 0b11100111;
    value |= (range << 3);
    mpuWriteRegister8(MPU6050_REG_ACCEL_CONFIG, value);
}

mpu6050_range_t GY87::mpuGetRange(void)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_ACCEL_CONFIG);
    value &= 0b00011000;
    value >>= 3;
    return (mpu6050_range_t)value;
}

void GY87::mpuSetDHPFMode(mpu6050_dhpf_t dhpf)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_ACCEL_CONFIG);
    value &= 0b11111000;
    value |= dhpf;
    mpuWriteRegister8(MPU6050_REG_ACCEL_CONFIG, value);
}

void GY87::mpuSetDLPFMode(mpu6050_dlpf_t dlpf)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_CONFIG);
    value &= 0b11111000;
    value |= dlpf;
    mpuWriteRegister8(MPU6050_REG_CONFIG, value);
}

void GY87::mpuSetClockSource(mpu6050_clockSource_t source)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_PWR_MGMT_1);
    value &= 0b11111000;
    value |= source;
    mpuWriteRegister8(MPU6050_REG_PWR_MGMT_1, value);
}

mpu6050_clockSource_t GY87::mpuGetClockSource(void)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_PWR_MGMT_1);
    value &= 0b00000111;
    return (mpu6050_clockSource_t)value;
}

bool GY87::mpuGetSleepEnabled(void)
{
    return mpuReadRegisterBit(MPU6050_REG_PWR_MGMT_1, 6);
}

void GY87::mpuSetSleepEnabled(bool state)
{
    mpuWriteRegisterBit(MPU6050_REG_PWR_MGMT_1, 6, state);
}

bool GY87::mpuGetIntZeroMotionEnabled(void)
{
    return mpuReadRegisterBit(MPU6050_REG_INT_ENABLE, 5);
}

void GY87::mpuSetIntZeroMotionEnabled(bool state)
{
    mpuWriteRegisterBit(MPU6050_REG_INT_ENABLE, 5, state);
}

bool GY87::mpuGetIntMotionEnabled(void)
{
    return mpuReadRegisterBit(MPU6050_REG_INT_ENABLE, 6);
}

void GY87::mpuSetIntMotionEnabled(bool state)
{
    mpuWriteRegisterBit(MPU6050_REG_INT_ENABLE, 6, state);
}

bool GY87::mpuGetIntFreeFallEnabled(void)
{
    return mpuReadRegisterBit(MPU6050_REG_INT_ENABLE, 7);
}

void GY87::mpuSetIntFreeFallEnabled(bool state)
{
    mpuWriteRegisterBit(MPU6050_REG_INT_ENABLE, 7, state);
}

uint8 GY87::mpuGetMotionDetectionThreshold(void)
{
    return mpuReadRegister8(MPU6050_REG_MOT_THRESHOLD);
}

void GY87::mpuSetMotionDetectionThreshold(uint8_t threshold)
{
    mpuWriteRegister8(MPU6050_REG_MOT_THRESHOLD, threshold);
}

uint8 GY87::mpuGetMotionDetectionDuration(void)
{
    return mpuReadRegister8(MPU6050_REG_MOT_DURATION);
}

void GY87::mpuSetMotionDetectionDuration(uint8 duration)
{
    mpuWriteRegister8(MPU6050_REG_MOT_DURATION, duration);
}

uint8 GY87::mpuGetZeroMotionDetectionThreshold(void)
{
    return mpuReadRegister8(MPU6050_REG_ZMOT_THRESHOLD);
}

void GY87::mpuSetZeroMotionDetectionThreshold(uint8 threshold)
{
    mpuWriteRegister8(MPU6050_REG_ZMOT_THRESHOLD, threshold);
}

uint8 GY87::mpuGetZeroMotionDetectionDuration(void)
{
    return mpuReadRegister8(MPU6050_REG_ZMOT_DURATION);
}

void GY87::mpuSetZeroMotionDetectionDuration(uint8 duration)
{
    mpuWriteRegister8(MPU6050_REG_ZMOT_DURATION, duration);
}

uint8 GY87::mpuGetFreeFallDetectionThreshold(void)
{
    return mpuReadRegister8(MPU6050_REG_FF_THRESHOLD);
}

void GY87::mpuSetFreeFallDetectionThreshold(uint8 threshold)
{
    mpuWriteRegister8(MPU6050_REG_FF_THRESHOLD, threshold);
}

uint8 GY87::mpuGetFreeFallDetectionDuration(void)
{
    return mpuReadRegister8(MPU6050_REG_FF_DURATION);
}

void GY87::mpuSetFreeFallDetectionDuration(uint8 duration)
{
    mpuWriteRegister8(MPU6050_REG_FF_DURATION, duration);
}

bool GY87::mpuGetI2CMasterModeEnabled(void)
{
    return mpuReadRegisterBit(MPU6050_REG_USER_CTRL, 5);
}

void GY87::mpuSetI2CMasterModeEnabled(bool state)
{
    mpuWriteRegisterBit(MPU6050_REG_USER_CTRL, 5, state);
}

void GY87::mpuSetI2CBypassEnabled(bool state)
{
    return mpuWriteRegisterBit(MPU6050_REG_INT_PIN_CFG, 1, state);
}

bool GY87::mpuGetI2CBypassEnabled(void)
{
    return mpuReadRegisterBit(MPU6050_REG_INT_PIN_CFG, 1);
}

void GY87::mpuSetAccelPowerOnDelay(mpu6050_onDelay_t delay)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_MOT_DETECT_CTRL);
    value &= 0b11001111;
    value |= (delay << 4);
    mpuWriteRegister8(MPU6050_REG_MOT_DETECT_CTRL, value);
}

mpu6050_onDelay_t GY87::mpuGetAccelPowerOnDelay(void)
{
    uint8 value;
    value = mpuReadRegister8(MPU6050_REG_MOT_DETECT_CTRL);
    value &= 0b00110000;
    return (mpu6050_onDelay_t)(value >> 4);
}

uint8_t GY87::mpuGetIntStatus(void)
{
    return mpuReadRegister8(MPU6050_REG_INT_STATUS);
}

Activites GY87::mpuReadActivites(void)
{
    uint8 data = mpuReadRegister8(MPU6050_REG_INT_STATUS);

    mpuA.isOverflow = ((data >> 4) & 1);
    mpuA.isFreeFall = ((data >> 7) & 1);
    mpuA.isInactivity = ((data >> 5) & 1);
    mpuA.isActivity = ((data >> 6) & 1);
    mpuA.isDataReady = ((data >> 0) & 1);

    data = mpuReadRegister8(MPU6050_REG_MOT_DETECT_STATUS);

    mpuA.isNegActivityOnX = ((data >> 7) & 1);
    mpuA.isPosActivityOnX = ((data >> 6) & 1);

    mpuA.isNegActivityOnY = ((data >> 5) & 1);
    mpuA.isPosActivityOnY = ((data >> 4) & 1);

    mpuA.isNegActivityOnZ = ((data >> 3) & 1);
    mpuA.isPosActivityOnZ = ((data >> 2) & 1);

    return mpuA;
}

Vector GY87::mpuReadRawAccel(void)
{
  Wire.beginTransmission(mpuAddress);
  Wire.write(MPU6050_REG_ACCEL_XOUT_H);
  Wire.endTransmission();

  Wire.beginTransmission(mpuAddress);
  Wire.requestFrom(mpuAddress, 6);

  while (Wire.available() < 6);

  uint8 xha = Wire.read();
  uint8 xla = Wire.read();
  uint8 yha = Wire.read();
  uint8 yla = Wire.read();
  uint8 zha = Wire.read();
  uint8 zla = Wire.read();

  mpuRa.XAxis = xha << 8 | xla;
  mpuRa.YAxis = yha << 8 | yla;
  mpuRa.ZAxis = zha << 8 | zla;

  return mpuRa;
}

Vector GY87::mpuReadNormalizeAccel(void)
{
    mpuReadRawAccel();

    mpuNa.XAxis = mpuRa.XAxis * mpuRangePerDigit * 9.80665f;
    mpuNa.YAxis = mpuRa.YAxis * mpuRangePerDigit * 9.80665f;
    mpuNa.ZAxis = mpuRa.ZAxis * mpuRangePerDigit * 9.80665f;

    return mpuNa;
}

Vector GY87::mpuReadScaledAccel(void)
{
    mpuReadRawAccel();

    mpuNa.XAxis = mpuRa.XAxis * mpuRangePerDigit;
    mpuNa.YAxis = mpuRa.YAxis * mpuRangePerDigit;
    mpuNa.ZAxis = mpuRa.ZAxis * mpuRangePerDigit;

    return mpuNa;
}


Vector GY87::mpuReadRawGyro(void)
{
  Wire.beginTransmission(mpuAddress);
  Wire.write(MPU6050_REG_GYRO_XOUT_H);
  Wire.endTransmission();

  Wire.beginTransmission(mpuAddress);
  Wire.requestFrom(mpuAddress, 6);

  while (Wire.available() < 6);

  uint8 xha = Wire.read();
  uint8 xla = Wire.read();
  uint8 yha = Wire.read();
  uint8 yla = Wire.read();
  uint8 zha = Wire.read();
  uint8 zla = Wire.read();

  mpuRg.XAxis = xha << 8 | xla;
  mpuRg.YAxis = yha << 8 | yla;
  mpuRg.ZAxis = zha << 8 | zla;

  return mpuRg;
}

Vector GY87::mpuReadNormalizeGyro(void)
{
    mpuReadRawGyro();

    if (mpuUseCalibrate)
    {
	mpuNg.XAxis = (mpuRg.XAxis - mpuDg.XAxis) * mpuDpsPerDigit;
	mpuNg.YAxis = (mpuRg.YAxis - mpuDg.YAxis) * mpuDpsPerDigit;
	mpuNg.ZAxis = (mpuRg.ZAxis - mpuDg.ZAxis) * mpuDpsPerDigit;
    } else
    {
	mpuNg.XAxis = mpuRg.XAxis * mpuDpsPerDigit;
	mpuNg.YAxis = mpuRg.YAxis * mpuDpsPerDigit;
	mpuNg.ZAxis = mpuRg.ZAxis * mpuDpsPerDigit;
    }

    if (mpuActualThreshold)
    {
	if (abs(mpuNg.XAxis) < mpuTg.XAxis) mpuNg.XAxis = 0;
	if (abs(mpuNg.YAxis) < mpuTg.YAxis) mpuNg.YAxis = 0;
	if (abs(mpuNg.ZAxis) < mpuTg.ZAxis) mpuNg.ZAxis = 0;
    }

    return mpuNg;
}

float GY87::mpuReadTemperature(void)
{
    int16 T;
    T = mpuReadRegister16(MPU6050_REG_TEMP_OUT_H);
    return (float)T/340 + 36.53;
}


int16 GY87::mpuGetGyroOffsetX(void)
{
    return mpuReadRegister16(MPU6050_REG_GYRO_XOFFS_H);
}

int16 GY87::mpuGetGyroOffsetY(void)
{
    return mpuReadRegister16(MPU6050_REG_GYRO_YOFFS_H);
}

int16 GY87::mpuGetGyroOffsetZ(void)
{
    return mpuReadRegister16(MPU6050_REG_GYRO_ZOFFS_H);
}

void GY87::mpuSetGyroOffsetX(int16 offset)
{
    mpuWriteRegister16(MPU6050_REG_GYRO_XOFFS_H, offset);
}

void GY87::mpuSetGyroOffsetY(int16 offset)
{
    mpuWriteRegister16(MPU6050_REG_GYRO_YOFFS_H, offset);
}

void GY87::mpuSetGyroOffsetZ(int16 offset)
{
    mpuWriteRegister16(MPU6050_REG_GYRO_ZOFFS_H, offset);
}

int16 GY87::mpuGetAccelOffsetX(void)
{
    return mpuReadRegister16(MPU6050_REG_ACCEL_XOFFS_H);
}

int16 GY87::mpuGetAccelOffsetY(void)
{
    return mpuReadRegister16(MPU6050_REG_ACCEL_YOFFS_H);
}

int16 GY87::mpuGetAccelOffsetZ(void)
{
    return mpuReadRegister16(MPU6050_REG_ACCEL_ZOFFS_H);
}

void GY87::mpuSetAccelOffsetX(int16 offset)
{
    mpuWriteRegister16(MPU6050_REG_ACCEL_XOFFS_H, offset);
}

void GY87::mpuSetAccelOffsetY(int16 offset)
{
    mpuWriteRegister16(MPU6050_REG_ACCEL_YOFFS_H, offset);
}

void GY87::mpuSetAccelOffsetZ(int16 offset)
{
    mpuWriteRegister16(MPU6050_REG_ACCEL_ZOFFS_H, offset);
}

// Calibrate algorithm
void GY87::mpuCalibrateGyro(uint8 samples)
{
    // Set calibrate
    mpuUseCalibrate = true;

    // Reset values
    float sumX = 0;
    float sumY = 0;
    float sumZ = 0;
    float sigmaX = 0;
    float sigmaY = 0;
    float sigmaZ = 0;

    // Read n-samples
    for (uint8 i = 0; i < samples; ++i)
    {
	mpuReadRawGyro();
	sumX += mpuRg.XAxis;
	sumY += mpuRg.YAxis;
	sumZ += mpuRg.ZAxis;

	sigmaX += mpuRg.XAxis * mpuRg.XAxis;
	sigmaY += mpuRg.YAxis * mpuRg.YAxis;
	sigmaZ += mpuRg.ZAxis * mpuRg.ZAxis;

	delay(5);
    }

    // Calculate delta vectors
    mpuDg.XAxis = sumX / samples;
    mpuDg.YAxis = sumY / samples;
    mpuDg.ZAxis = sumZ / samples;

    // Calculate threshold vectors
    mpuTh.XAxis = sqrt((sigmaX / 50) - (mpuDg.XAxis * mpuDg.XAxis));
    mpuTh.YAxis = sqrt((sigmaY / 50) - (mpuDg.YAxis * mpuDg.YAxis));
    mpuTh.ZAxis = sqrt((sigmaZ / 50) - (mpuDg.ZAxis * mpuDg.ZAxis));

    // If already set threshold, recalculate threshold vectors
    if (mpuActualThreshold > 0)
    {
	mpuSetThreshold(mpuActualThreshold);
    }
}

// Get current threshold value
uint8 GY87::mpuGetThreshold(void)
{
    return mpuActualThreshold;
}

// Set treshold value
void GY87::mpuSetThreshold(uint8 multiple)
{
    if (multiple > 0)
    {
	// If not calibrated, need calibrate
	if (!mpuUseCalibrate)
	{
	    mpuCalibrateGyro();
	}

	// Calculate threshold vectors
	mpuTg.XAxis = mpuTh.XAxis * multiple;
	mpuTg.YAxis = mpuTh.YAxis * multiple;
	mpuTg.ZAxis = mpuTh.ZAxis * multiple;
    } else
    {
	// No threshold
	mpuTg.XAxis = 0;
	mpuTg.YAxis = 0;
	mpuTg.ZAxis = 0;
    }

    // Remember old threshold value
    mpuActualThreshold = multiple;
}

// Fast read 8-bit from register
uint8_t GY87::mpuFastRegister8(uint8 reg)
{
  uint8 value;

  Wire.beginTransmission(mpuAddress);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.beginTransmission(mpuAddress);
  Wire.requestFrom(mpuAddress, 1);
  value = Wire.read();
  Wire.endTransmission();

  return value;
}

// Read 8-bit from register
uint8 GY87::mpuReadRegister8(uint8 reg)
{
  uint8 value;

  Wire.beginTransmission(mpuAddress);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.beginTransmission(mpuAddress);
  Wire.requestFrom(mpuAddress, 1);
  while(!Wire.available()) {};
  value = Wire.read();
  Wire.endTransmission();

  return value;
}

// Write 8-bit to register
void GY87::mpuWriteRegister8(uint8 reg, uint8 value)
{
  Wire.beginTransmission(mpuAddress);

  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}

int16 GY87::mpuReadRegister16(uint8 reg)
{
  int16 value;
  Wire.beginTransmission(mpuAddress);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.beginTransmission(mpuAddress);
  Wire.requestFrom(mpuAddress, 2);
  while(!Wire.available()) {};
  uint8 vha = Wire.read();
  uint8 vla = Wire.read();
  Wire.endTransmission();

  value = vha << 8 | vla;

  return value;
}

void GY87::mpuWriteRegister16(uint8 reg, int16 value)
{
  Wire.beginTransmission(mpuAddress);
  Wire.write(reg);
  Wire.write((uint8)(value >> 8));
  Wire.write((uint8)value);
  Wire.endTransmission();
}

// Read register bit
bool GY87::mpuReadRegisterBit(uint8 reg, uint8 pos)
{
    uint8 value;
    value = mpuReadRegister8(reg);
    return ((value >> pos) & 1);
}

// Write register bit
void GY87::mpuWriteRegisterBit(uint8 reg, uint8 pos, bool state)
{
    uint8 value;
    value = mpuReadRegister8(reg);

    if (state)
    {
        value |= (1 << pos);
    } else 
    {
        value &= ~(1 << pos);
    }

    mpuWriteRegister8(reg, value);
}






bool GY87::magBegin()
{
    //Wire.begin();

    if ((magFastRegister8(HMC5883L_REG_IDENT_A) != 0x48)
    || (magFastRegister8(HMC5883L_REG_IDENT_B) != 0x34)
    || (magFastRegister8(HMC5883L_REG_IDENT_C) != 0x33))
    {
	return false;
    }

    magSetRange(HMC5883L_RANGE_1_3GA);
    magSetMeasurementMode(HMC5883L_CONTINOUS);
    magSetDataRate(HMC5883L_DATARATE_15HZ);
    magSetSamples(HMC5883L_SAMPLES_1);

    magGPerDigit = 0.92f;

    return true;
}

Vector GY87::magReadRaw(void)
{
    magMV.XAxis = magReadRegister16(HMC5883L_REG_OUT_X_M) - magXOffset;
    magMV.YAxis = magReadRegister16(HMC5883L_REG_OUT_Y_M) - magYOffset;
    magMV.ZAxis = magReadRegister16(HMC5883L_REG_OUT_Z_M);

    return magMV;
}

Vector GY87::magReadNormalize(void)
{
    magMV.XAxis = ((float)magReadRegister16(HMC5883L_REG_OUT_X_M) - magXOffset) * magGPerDigit;
    magMV.YAxis = ((float)magReadRegister16(HMC5883L_REG_OUT_Y_M) - magYOffset) * magGPerDigit;
    magMV.ZAxis = (float)magReadRegister16(HMC5883L_REG_OUT_Z_M) * magGPerDigit;

    return magMV;
}

void GY87::magSetOffset(int xo, int yo)
{
    magXOffset = xo;
    magYOffset = yo;
}

void GY87::magSetRange(hmc5883l_range_t range)
{
    switch(range)
    {
	case HMC5883L_RANGE_0_88GA:
	    magGPerDigit = 0.073f;
	    break;

	case HMC5883L_RANGE_1_3GA:
	    magGPerDigit = 0.92f;
	    break;

	case HMC5883L_RANGE_1_9GA:
	    magGPerDigit = 1.22f;
	    break;

	case HMC5883L_RANGE_2_5GA:
	    magGPerDigit = 1.52f;
	    break;

	case HMC5883L_RANGE_4GA:
	    magGPerDigit = 2.27f;
	    break;

	case HMC5883L_RANGE_4_7GA:
	    magGPerDigit = 2.56f;
	    break;

	case HMC5883L_RANGE_5_6GA:
	    magGPerDigit = 3.03f;
	    break;

	case HMC5883L_RANGE_8_1GA:
	    magGPerDigit = 4.35f;
	    break;

	default:
	    break;
    }

    magWriteRegister8(HMC5883L_REG_CONFIG_B, range << 5);
}

hmc5883l_range_t GY87::magGetRange(void)
{
    return (hmc5883l_range_t)((magReadRegister8(HMC5883L_REG_CONFIG_B) >> 5));
}

void GY87::magSetMeasurementMode(hmc5883l_mode_t mode)
{
    uint8_t value;

    value = magReadRegister8(HMC5883L_REG_MODE);
    value &= 0b11111100;
    value |= mode;

    magWriteRegister8(HMC5883L_REG_MODE, value);
}

hmc5883l_mode_t GY87::magGetMeasurementMode(void)
{
    uint8_t value;

    value = magReadRegister8(HMC5883L_REG_MODE);
    value &= 0b00000011;

    return (hmc5883l_mode_t)value;
}

void GY87::magSetDataRate(hmc5883l_dataRate_t dataRate)
{
    uint8_t value;

    value = magReadRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b11100011;
    value |= (dataRate << 2);

    magWriteRegister8(HMC5883L_REG_CONFIG_A, value);
}

hmc5883l_dataRate_t GY87::magGetDataRate(void)
{
    uint8_t value;

    value = magReadRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b00011100;
    value >>= 2;

    return (hmc5883l_dataRate_t)value;
}

void GY87::magSetSamples(hmc5883l_samples_t samples)
{
    uint8_t value;

    value = magReadRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b10011111;
    value |= (samples << 5);

    magWriteRegister8(HMC5883L_REG_CONFIG_A, value);
}

hmc5883l_samples_t GY87::magGetSamples(void)
{
    uint8_t value;

    value = magReadRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b01100000;
    value >>= 5;

    return (hmc5883l_samples_t)value;
}

// Write byte to register
void GY87::magWriteRegister8(uint8_t reg, uint8_t value)
{
  Wire.beginTransmission(HMC5883L_ADDRESS);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}

// Read byte to register
uint8_t GY87::magFastRegister8(uint8_t reg)
{
  uint8_t value;
  Wire.beginTransmission(HMC5883L_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.requestFrom(HMC5883L_ADDRESS, 1);
  value = Wire.read();
  Wire.endTransmission();

  return value;
}

// Read byte from register
uint8_t GY87::magReadRegister8(uint8_t reg)
{
  uint8_t value;
  Wire.beginTransmission(HMC5883L_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();

  Wire.beginTransmission(HMC5883L_ADDRESS);
  Wire.requestFrom(HMC5883L_ADDRESS, 1);
  while(!Wire.available()) {};
  value = Wire.read();
  Wire.endTransmission();

  return value;
}

// Read word from register
int16 GY87::magReadRegister16(uint8 reg)
{
    int16 value;
    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();

    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.requestFrom(HMC5883L_ADDRESS, 2);
    while(!Wire.available()) {};
    uint8_t vha = Wire.read();
    uint8_t vla = Wire.read();
    Wire.endTransmission();

    value = vha << 8 | vla;

    return value;
}
