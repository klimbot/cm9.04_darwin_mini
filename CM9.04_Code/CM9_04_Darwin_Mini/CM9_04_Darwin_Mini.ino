// WORKING
//    * setting servo PID tuning
//    * adjustment of lean forward based on play speed
//    * insertion of motions if a motion is currently playing
// KNOWN BUGS
//    * the lean foward is not dependent on the play speed, just the current lean forward. should be tied
//    * robot shakes after programming if battery power is on 

// Define the hardware we are using in the bot
#define DXL_BUS_SERIAL1 1  //Dynamixel on Serial1(USART1)  <-OpenCM9.04
#define XL320
#define GY87_IMU

// Pick the debugging interface by changing the interface value to 1
#define USB 1
#define BLUETOOTH 0
#define WIFI 0

// Set definitions to enable debugging
//#define DEBUG_DARWIN
//#define DEBUG_ACCEL
//#define DEBUG_GYRO
//#define DEBUG_MAG


#include <CM9_BC.h>
#include "HelloRobo_RPM.h"
#include "dynamixel_type.h"

// If we have a GY87 IMU include the files we need to run it
#ifdef GY87_IMU
#include <Wire.h>
#include <GY87.h>
#endif GY87_IMU

#ifdef GY87_IMU
GY87 darwinIMU;
#endif GY87_IMU
Dynamixel Dxl(DXL_BUS_SERIAL1);
Dynamixel *pDxl = &Dxl;

// Adjust for each bot
int Offset_Calibration[] = {18,0,0,0,0,0,0,0,0,3,0,0,0,3,-3,3,0,0,0};
// Sensor offset for dynamic adjustment
int Offset_Accelerometer[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
// Sensor offset for dynamic adjustment
int Offset_Gyro[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
// Sensor offset for dynamic adjustment
int Offset_Magnetometer[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
// Other adjustment factor
int Offset_Misc[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int miscCOGFactorMax = 6;
int miscCOGFactor = 2;
int ledColour = 0;
boolean GyroStabilised = 0;
int fallenCount = 0;

BioloidController DarwinMini;
#define UPDATE_RATE_ACCEL_MAG 500000  // defining how often we will update the accelerometer and magnetometer readings
#define UPDATE_RATE_GYRO 50000        // defining how often we will update the gyro readings
#define UPDATE_RATE_PLAY 50000        // defining how often we will update the gyro readings
HardwareTimer TimerPlay(2);
HardwareTimer TimerAccel(3);
HardwareTimer TimerGyro(4);


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setServoTuningParameters(int servoID) {
  Dxl.writeByte(servoID, P_GAIN, 64);
  Dxl.writeByte(servoID, I_GAIN, 0);
  Dxl.writeByte(servoID, D_GAIN, 0);
}

void motionUpdate(void) {
#ifdef DEBUG_DARWIN
  printMessage("PLAYING");
#endif DEBUG_DARWIN
  DarwinMini.Play();
}

boolean printMessage(char* message) {
  if (USB) {
    SerialUSB.println(message);
    return true;
  }
  else if (BLUETOOTH) {
    Serial2.println(message);
    return true;
  }
  else
    return false;
}

boolean printMessage(int number) {
  if (USB) {
    SerialUSB.println(number);
    return true;
  }
  else if (BLUETOOTH) {
    Serial2.println(number);
    return true;
  }
  else
    return false;
}

void insertMotionDefaultSpeed(unsigned int motionNumber) {
  // Check currently playing motion and current play speed
  unsigned int currentMotionNumber = DarwinMini.MotionPage();
  unsigned int playSpeed = DarwinMini.getTimeModifier();
	
  // Set the new motion to play
  DarwinMini.MotionPage(motionNumber);
  DarwinMini.setTimeModifier(100);

  // Wait till the new motion is done
  while(DarwinMini.MotionStatus()) { DarwinMini.Play(); }
	
  // Set the motion speed back to the original and play the original motion
  DarwinMini.setTimeModifier(playSpeed);
  DarwinMini.MotionPage(currentMotionNumber);
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setup() {
  // Dynamixel 2.0 Baudrate -> 0: 9600, 1: 57600, 2: 115200, 3: 1Mbps 
  Dxl.begin(3);
  Dxl.setLibNumberTxRxAttempts(3);

  setServoTuningParameters(BROADCAST_ID);
  Dxl.jointMode(BROADCAST_ID);
  
  // Setup Dynamixel bus, USB and Bluetooth interfaces
  SerialUSB.begin();
  Serial2.begin(57600);

  while (1) {
    delay(500);
    digitalWrite(BOARD_LED_PIN, LOW);
    SerialUSB.print("Send any value to continue...\n");
    
    delay(500);
    digitalWrite(BOARD_LED_PIN, HIGH);
	
    if (SerialUSB.available()) {
      int val = SerialUSB.read();
      if (val == 'I') 
      break;
    }
    if (Serial2.available()) {
      int val = Serial2.read();
      if (val == 'I') 
      break;
    }
    /*if (Serial3.available() {
      int val = Serial3.read();
      if (val == 'I') 
      break;
    }*/
  }

  SerialUSB.print("\n\nStarting program\n");
  // Load the RoboPlusMotion_Array from our _RPM header file
  DarwinMini.RPM_Setup(HelloRobo_RoboPlusMotion_Array);
  //  This gives the motion engine the location of the RPM page file
  //  Sets up the motion engine (allocates memory and initializes variables)
  //  Loads the servo IDs from the first pose in the first sequence in the array
  SerialUSB.print("Motion engine setup complete.\n");
  
  // Set up the Motion Play interrupt
  //TimerPlay.pause();                                      // Pause the timer while we're configuring it
  //TimerPlay.setPeriod(UPDATE_RATE_PLAY);                  // Set up period in microseconds
  //TimerPlay.setMode(TIMER_CH2, TIMER_OUTPUT_COMPARE);     // Set up an interrupt on channel 1
  //TimerPlay.setCompare(TIMER_CH2, 1);                     // Interrupt 1 count after each update
  //TimerPlay.attachInterrupt(TIMER_CH2, motionUpdate);       // Set the interrupt call routines
  //TimerPlay.refresh();                                    // Refresh the timer's count, prescale, and overflow
  //TimerPlay.resume();
  
  // Set up the GY87 IMU
#ifdef GY87_IMU
  SerialUSB.println("Initialising GY87 IMU");
  Wire.begin(25,24);    //i2c bus init SDA->25, SCL->24
  while(!darwinIMU.gy87Begin()) { delay(500); }
  
  // Set the IMU calibration and offsets
  //darwinIMU.magSetOffset(0,0);
  
  // Set up interrupt driven IMU updates
  SerialUSB.print("Attaching interrupts for IMU functions\n");
  TimerGyro.pause();                                      // Pause the timer while we're configuring it
  TimerGyro.setPeriod(UPDATE_RATE_GYRO);                  // Set up period in microseconds
  TimerGyro.setMode(TIMER_CH4, TIMER_OUTPUT_COMPARE);     // Set up an interrupt on channel 1
  TimerGyro.setCompare(TIMER_CH4, 1);                     // Interrupt 1 count after each update
  TimerGyro.attachInterrupt(TIMER_CH4, updateGyro);       // Set the interrupt call routines
  TimerGyro.refresh();                                    // Refresh the timer's count, prescale, and overflow
  TimerGyro.resume();                                     // Start the timer counting

  TimerAccel.pause();                                     // Pause the timer while we're configuring it
  TimerAccel.setPeriod(UPDATE_RATE_ACCEL_MAG);            // Set up period in microseconds
  TimerAccel.setMode(TIMER_CH3, TIMER_OUTPUT_COMPARE);    // Set up an interrupt on channel 1
  TimerAccel.setCompare(TIMER_CH3, 1);                    // Interrupt 1 count after each update
  TimerAccel.attachInterrupt(TIMER_CH3, updateAccelMag);  // Set the interrupt call routines
  TimerAccel.refresh();                                   // Refresh the timer's count, prescale, and overflow
  TimerAccel.resume();                                    // Start the timer counting
#endif GY87_IMU
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void loop() {
  // Heartbeat
  unsigned int heartbeat = millis();

  // Set bot to an initial/ready position
  printMessage("loop() started. Going to Ready Position...\n");

  // Load the offsets
  DarwinMini.loadOffsets(Offset_Calibration);
  //DarwinMini.loadAccelerometer(Offset_Accelerometer);
  //DarwinMini.loadGyroscope(Offset_Gyro);
  //DarwinMini.loadMagnetometer(Offset_Magnetometer);
  //DarwinMini.loadMiscellaneous(Offset_Misc);

  // Load the initial stance motion page
  DarwinMini.MotionPage(InitialPose);

  while (DarwinMini.MotionStatus()) {
    delay(1);
    DarwinMini.Play();

    if ((millis()-heartbeat)>200) {
      heartbeat = 0;
#ifdef DEBUG_DARWIN
      printMessage(".");
#endif DEBUG_DARWIN
    }
  }

  printMessage(" done.\nStarting main loop for user code.\n");

  /// Main loop where all user code occurs
  //  Do not modify anthing outside this while loop unless you are very, very
  //  certain of what you are doing.
  //  Do not create any infinite loops or >30ms waits within this loop, or the
  //  motion engine will fail to update.
  while(1){     
    unsigned int rang = 0;
    
    if (Serial2.available()) { rang = Serial2.read(); }
    else if (SerialUSB.available()) { rang = SerialUSB.read(); }
    		
    // ARROW KEY OPTIONS
    if (rang == 'I') { DarwinMini.MotionPage(InitialPose); }
    else if (rang == 'Z') { DarwinMini.MotionPage(InitalPoseCont); }
    else if (rang == 'S') { DarwinMini.MotionPage(Advance); }
    else if (rang == 'F') { DarwinMini.MotionPage(FastAdvance); }
    else if (rang == 'Q') { DarwinMini.MotionPage(FastAdvanceLeft); }
    else if (rang == 'T') { DarwinMini.MotionPage(FastAdvanceRight); }
    else if (rang == 'B') { DarwinMini.MotionPage(Reverse); }
    else if (rang == 'L') { insertMotionDefaultSpeed(LeftTurn); }
    else if (rang == 'R') { insertMotionDefaultSpeed(RightTurn); }
    else if (rang == 'i') { insertMotionDefaultSpeed(RightSideStep); }
    else if (rang == 'j') { insertMotionDefaultSpeed(LeftSideStep); }
    
    // ACTION KEY OPTIONS
    else if (rang == 'U') { insertMotionDefaultSpeed(GetUp); }
    else if (rang == 'G') { insertMotionDefaultSpeed(Greet1); }
    else if (rang == 'g') { insertMotionDefaultSpeed(Greet2); }
    else if (rang == 'l') { insertMotionDefaultSpeed(LeftKick); }
    else if (rang == 'r') { insertMotionDefaultSpeed(RightKick); }
    else if (rang == 'H') { insertMotionDefaultSpeed(LeftHook); }
    else if (rang == 'h') { insertMotionDefaultSpeed(RightHook); }
    else if (rang == 'x') { GyroStabilised = !GyroStabilised; }
    else if (rang == '|') {
      ledColour += 1;
      
      if (ledColour == 8)
        ledColour = 0;	
        
      // Toggle LED Colours
      Dxl.writeWord(BROADCAST_ID, LED, ledColour);
    }
    else if (rang == '+') {
      // Get the current Play Speed
      unsigned int currentPlaySpeed = DarwinMini.getTimeModifier();
      unsigned int newPlaySpeed = currentPlaySpeed - 10;
      
      // 50 <= timeModifier <= 100
      if (newPlaySpeed < 50) {
        Offset_Misc[13] = (int)(100 - 50)/10;
        Offset_Misc[14] = (int)(100 - 50)/(-10);
      }
      else if ((newPlaySpeed >= 50) && (newPlaySpeed <= 100)) {
        Offset_Misc[13] = (int)(100 - newPlaySpeed)/10;
        Offset_Misc[14] = (int)(100 - newPlaySpeed)/(-10);
      }
      else {
        Offset_Misc[13] = 0;
        Offset_Misc[14] = 0;
      }
      
      // Increase Play speed
      DarwinMini.setTimeModifier(newPlaySpeed);			
      
      // Set the misc offsets
      DarwinMini.loadMiscellaneous(Offset_Misc);
    }
    else if (rang == '-') {
      // Get the current Play Speed
      unsigned int currentPlaySpeed = DarwinMini.getTimeModifier();
      unsigned int newPlaySpeed = currentPlaySpeed + 10;
      
      // 50 <= timeModifier <= 100
      if (newPlaySpeed < 50) {
        Offset_Misc[13] = (int)(100 - 50)/(-10);
        Offset_Misc[14] = (int)(100 - 50)/10;
      }
      else if ((newPlaySpeed >= 50) && (newPlaySpeed <= 100)) {
        Offset_Misc[13] = (int)(100 - newPlaySpeed)/(-10);
        Offset_Misc[14] = (int)(100 - newPlaySpeed)/10;
      }
      else {
        Offset_Misc[13] = 0;
        Offset_Misc[14] = 0;
      }
        
      // Increase Play speed
      DarwinMini.setTimeModifier(newPlaySpeed);
    }
    else if (rang == '*') {
      // Reset the time modifier to default
      DarwinMini.setTimeModifier(100);
#ifdef DEBUG_DARWIN
      printMessage("Resetting motion speed");
#endif DEBUG_DARWIN
    }
              
    // INVALID/STOP
    else if (rang > 0) { DarwinMini.MotionPage(InitialPose); }
    
    // CHECK ROBOT STATE
    if (fallenCount >= 4) { insertMotionDefaultSpeed(GetUp); fallenCount = 0;}
			
    if ((millis()-heartbeat)>200) {
      heartbeat = millis();
      
#ifdef DEBUG_DARWIN
      SerialUSB.print(".");

      if (DarwinMini.MotionStatus()) {
        if (DarwinMini.MotionPage()>0) {
          printMessage("Playing page: ");
          printMessage(DarwinMini.MotionPage());
        }
      }
#endif DEBUG_DARWIN
    }

    DarwinMini.Play();	// TODO: replace with timer interrupt-based update.
  }
}




void insertMotion(unsigned int motionNumber) {
  // Check currently playing motion
  unsigned int currentMotionNumber = DarwinMini.MotionPage();
	
  DarwinMini.MotionPage(motionNumber);
	
  // Wait till the motion is done
  while(DarwinMini.MotionStatus()) { DarwinMini.Play(); }
	
  DarwinMini.MotionPage(currentMotionNumber);
}

#ifdef GY87_IMU
void updateAccelMag(void) {
  Vector accelRaw = darwinIMU.mpuReadRawAccel();
  Vector magRaw = darwinIMU.magReadRaw();
  
  //Automatically get up if we have fallen down
  if (accelRaw.ZAxis < 14000) {
    printMessage("Robot has fallen");
    fallenCount += 1;
  }
  else
    fallenCount = 0;
  
#ifdef DEBUG_ACCEL
  SerialUSB.print(" Accel X: ");SerialUSB.print(accelRaw.XAxis);
  SerialUSB.print("| Accel Y: ");SerialUSB.print(accelRaw.YAxis);
  SerialUSB.print("| Accel Z: ");SerialUSB.print(accelRaw.ZAxis);
  SerialUSB.print(" Fallen Count: ");SerialUSB.print(fallenCount);
  SerialUSB.println();
#endif DEBUG_ACCEL
#ifdef DEBUG_MAG
  SerialUSB.print("| Mag X: ");SerialUSB.print(magRaw.XAxis);
  SerialUSB.print("| Mag Y: ");SerialUSB.print(magRaw.YAxis);
  SerialUSB.print("| Mag Z: ");SerialUSB.print(magRaw.ZAxis);
  SerialUSB.println();
#endif DEBUG_MAG
}
#endif GY87_IMU

#ifdef GY87_IMU
void updateGyro(void) {
  // Read the sensor values from the IMU
  Vector gyroRaw = darwinIMU.mpuReadRawGyro();

#ifdef DEBUG_GYRO
  SerialUSB.print("getGyro X: ");SerialUSB.print(gyroRaw.XAxis);
  SerialUSB.print(" | getGyro Y: ");SerialUSB.print(gyroRaw.YAxis);
  SerialUSB.print(" | getGyro Z: ");SerialUSB.print(gyroRaw.ZAxis);
  SerialUSB.println();
#endif DEBUG_GYRO

  //Update the joint offsets based on gyro and accelerometer readings
  Offset_Gyro[13] = (int)(gyroRaw.YAxis*-1.0);
  Offset_Gyro[14] = (int)(gyroRaw.YAxis*1.0);
	
  // Set the misc offsets
  if (GyroStabilised) {
    //DarwinMini.loadGyroscope(Offset_Gyro);
  }
}
#endif GY87_IMU

boolean printRobotActions() {
  printMessage("\nRobot Options");
  printMessage("\tS: Advance");
  printMessage("\tF: Fast Advance");
  printMessage("\tQ: Fast Advance Left");
  printMessage("\tT: Fast Advance Right");
  printMessage("\tB: Reverse");
  printMessage("\tL: Turn Left");
  printMessage("\tR: Turn Right");
  printMessage("\ti: Right Side Step");
  printMessage("\tj: Left Side Step");
  printMessage("\tU: Get Up");
  printMessage("\tG: Greeting 1");
  printMessage("\tg: Greeting 2");
  printMessage("\tl: Left Kick");
  printMessage("\tr: Right Kick");
  printMessage("\tH: Left Hook");
  printMessage("\th: Right Hook");
  printMessage("\t|: Change LED Colour");
  printMessage("\t+ Increase Motion Speed");
  printMessage("\t-: Decrease Motion Speed");
  printMessage("\t*: Reset Motion Speed");

  return true;
}

