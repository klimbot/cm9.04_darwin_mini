// XL-320 address table

#ifdef XL320
enum{ 
  MAX_TORQUE_L = 15,
  LED = 25,
  D_GAIN = 27,
  I_GAIN = 28,
  P_GAIN = 29,
  GOAL_TORQUE_L = 35,
  PUNCH_L = 51
};
#endif

