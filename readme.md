Requires ROBOTIS OpenCM: http://support.robotis.com/en/software/robotis_opencm/robotis_opencm.htm

To compile the CM9_04 .ino files first copy the CM9_BC files from the root code directory to the ROBOTIS OpenCM libraries folder and overwrite the existing CM9_BC files
eg: move the two files CM9.04_Code\CM9_BC.cpp and CM9.04_Code\CM9_BC.h to 'ROBOTIS-v1.0.4-windows\libraries\CM9_BC\'

